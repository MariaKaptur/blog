<?php

namespace App\Repository;

use App\Entity\MkAdmin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method MkAdmin|null find($id, $lockMode = null, $lockVersion = null)
 * @method MkAdmin|null findOneBy(array $criteria, array $orderBy = null)
 * @method MkAdmin[]    findAll()
 * @method MkAdmin[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MkAdminRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MkAdmin::class);
    }

    // /**
    //  * @return MkAdmin[] Returns an array of MkAdmin objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MkAdmin
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
